# Makefile for schedulers

CC=gcc
RM=rm -f
VALGRIND=valgrind -s --leak-check=full --show-leak-kinds=all

DEPS=process.o queue.o
OBJS=$(DEPS) rr-scheduler.o fcfs-scheduler.o priority-scheduler.o sjf-scheduler.o sjrf-scheduler.o
HEADERS=process.h queue.h rr-scheduler.h fcfs-scheduler.h priority-scheduler.h sjf-scheduler.h sjrf-scheduler.h
FLAGS=-Wall -Werror -Wpedantic -std=gnu11 -g
LDFLAGS=-pthread

schedulers: $(OBJS) $(HEADERS)
	$(CC) $(FLAGS) schedulers.c -o schedulers $(LDFLAGS) $(OBJS)

fcfs-scheduler.o: fcfs-scheduler.c fcfs-scheduler.h $(DEPS)
	$(CC) $(FLAGS) fcfs-scheduler.c -c

priority-scheduler.o: priority-scheduler.c priority-scheduler.h $(DEPS)
	$(CC) $(FLAGS) priority-scheduler.c -c

sjf-scheduler.o: sjf-scheduler.c sjf-scheduler.h $(DEPS)
	$(CC) $(FLAGS) sjf-scheduler.c -c

rr-scheduler.o: rr-scheduler.c rr-scheduler.h $(DEPS)
	$(CC) $(FLAGS) rr-scheduler.c -c

sjrf-scheduler.o: sjrf-scheduler.c  sjrf-scheduler.h $(DEPS)
	$(CC) $(FLAGS) sjrf-scheduler.c -c

queue.o: queue.c queue.h process.o
	$(CC) $(FLAGS) -c queue.c

process.o: process.c process.h
	$(CC) $(FLAGS) -c process.c

clean:
	$(RM) schedulers
	$(RM) *.o

.PHONY: clean schedulers
