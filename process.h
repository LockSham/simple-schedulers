// SPDX-License-Identifier: MIT
#ifndef PROCESS_H_
#define PROCESS_H_

typedef struct process
{
	char data;
	int burst;
	int priority;
	struct process *next;
}node;

node * init_process_node(char data, int burst);

node *init_priority_process_node(char data, int burst, int priority);

void free_processes(node **head);

#endif
