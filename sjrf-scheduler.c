// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include "process.h"
#include "queue.h"
#include "sjrf-scheduler.h"

pthread_mutex_t queue_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ready_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t read_cond  = PTHREAD_COND_INITIALIZER;

void sjrf_scheduler(Queue *ready, Queue *dispatch)
{
	if (!ready || !dispatch) {
		return;
	}

	while (ready->head) {
		pthread_mutex_lock(&queue_mut);

		node *p = ready->head;
		node *max_p = ready->head;
		node *parent_max = ready->head;
		while (p->next) {
			if (p->next->burst < max_p->burst) {
				max_p = p->next;
				parent_max = p;
			}
			p = p->next;
		}

		if (max_p == ready->head) { // Max priority is head
			ready->head = max_p->next;
		}

		if (max_p == ready->tail) { // Max priotiry is ready queues tail
			ready->tail = parent_max;
		}

		node *dp = calloc(1, sizeof(node));
		memcpy(dp, max_p, sizeof(node));
		dp->next = NULL;

		parent_max->next = max_p->next;
		printf("dispatching (%c, %d)\n", dp->data, dp->burst);
		queue_process(dispatch, dp);

		struct timeval now;
		struct timespec timeout;
		gettimeofday(&now, NULL);
		timeout.tv_sec = now.tv_sec + (max_p->burst/100);
		timeout.tv_nsec = now.tv_usec * 1000;

		free(max_p);
		max_p = NULL;

		pthread_mutex_unlock(&queue_mut);
		pthread_cond_timedwait(&read_cond, &ready_mut, &timeout);
		printf("PREEMPTED\n");
	}

	return;
}

struct thread_data {
	long tid;
	Queue *ready;
	Queue *dispatch;
};

void *thread_runner(void *param)
{
	struct thread_data *data = (struct thread_data*)param;
	sjrf_scheduler(data->ready, data->dispatch);
	pthread_exit(0);
}


int run_sjrf_scheduler(void)
{
	pthread_t thread;

	Queue *ready = init_queue();
	Queue *dispatch = init_queue();
	if (!ready || !dispatch ) {
		fprintf(stderr, "ERROR: Could not initialise ready/dispatch queue\n");
		return -1;
	}

	int burst = 0;
	node *process;
	for (int i = 1; i<6; i++) {
		burst = (int)((double)(99)*rand()/(999999999+1.0));
		process = init_process_node(64+i, burst);
		queue_process(ready, process);
	}

	printf("The ready queue for the SJRF scheduler algorithm is \n\n");
	print_queue(ready);
	printf("\n\n");

	struct thread_data data = {0, ready, dispatch};
	pthread_create(&thread, NULL, thread_runner, (void*)&data);
	//sjrf_scheduler(ready, dispatch);

	for (int i = 7; i<12; i++) {
		burst = (int)((double)(99)*rand()/(999999999+1.0));
		process = init_process_node(64+i, burst);
		printf("ADDING (%c, %d)\n", process->data, process->burst);
		pthread_mutex_lock(&queue_mut);
		queue_process(ready, process);
		pthread_mutex_unlock(&queue_mut);
		pthread_cond_signal(&read_cond);
		sleep(1);
	}

	pthread_join(thread, NULL);

	printf("The dispatch queue for the SJRF scheduler algorithm is \n\n");
	print_queue(dispatch);

	printf("\n\nEnding program\n");
	del_queue(&ready);
	del_queue(&dispatch);

	pthread_exit(0);
	return 0;
}
