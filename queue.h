// SPDX-License-Identifier: MIT
#ifndef QUEUE_H_
#define QUEUE_H_
#include "process.h"

typedef struct queue
{
	node *head;
	node *tail;
}Queue;

Queue *init_queue();

void del_queue(Queue **queue);

void queue_process(Queue *queue, node *process);

node *dqueue_process(Queue *queue);

void print_queue(Queue *queue);

#endif
