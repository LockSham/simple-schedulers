// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "process.h"
#include "queue.h"
#include "priority-scheduler.h"

void priority_scheduler(Queue *ready, Queue *dispatch)
{
	if (!ready || !dispatch) {
		return;
	}

	while (ready->head) {
		node *p = ready->head;
		node *max_p = ready->head;
		node *parent_max = ready->head;
		while (p->next) {
			if (p->next->priority < max_p->priority) {
				max_p = p->next;
				parent_max = p;
			}
			p = p->next;
		}

		if (max_p == ready->head) { // Max priority is head
			ready->head = max_p->next;
		}

		node *dp = calloc(1, sizeof(node));
		memcpy(dp, max_p, sizeof(node));
		dp->next = NULL;
		queue_process(dispatch, dp);

		parent_max->next = max_p->next;
		free(max_p);
	}
	return;
}

int run_priority_scheduler(void)
{
	Queue *ready = init_queue();
	Queue *dispatch = init_queue();
	if (!ready || !dispatch ) {
		fprintf(stderr, "ERROR: Could not initialise ready/dispatch queue\n");
		return -1;
	}

	int priorities[] = {3, 1, 4, 5, 2, 0};
	int burst = 0;
	node *process;
	for (int i = 1; i<=6; i++) {
		burst = (int)((double)(99)*rand()/(999999999+1.0));
		process = init_priority_process_node(64+i, burst, priorities[i-1]);
		queue_process(ready, process);
	}
	printf("The ready queue for the priority scheduler algorithm is \n\n");
	print_queue(ready);
	printf("\n\n");

	priority_scheduler(ready, dispatch);

	printf("The dispatch queue for the priority scheduler algorithm is \n\n");
	print_queue(dispatch);

	printf("Ending program\n");
	del_queue(&ready);
	del_queue(&dispatch);
	return 0;
}
