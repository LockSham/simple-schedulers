// SPDX-License-Identifier: MIT
#ifndef FCFS_SCHEDULER_H_
#define FCFS_SCHEDULER_H_

int run_fcfs_scheduler(void);

#endif
