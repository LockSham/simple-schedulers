## Schedulers
A small simple implementation of the scheduling algorithms in `Operating system Concepts ninth edition` chapter 6 section 6.3 Scheduling Algorithms

## Building
Schedulers uses make and requires pthreads

Building is as easy as cloning the current master branch and running make.

```
git clone https://gitlab.com/LockSham/simple-schedulers simple-schedulers
cd simple-schedulers
make
```

## Usage
```
./schedulers -s SCHEDULER (One of (fcfs, priority, rr, sjf, sjrf))
```
