// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include "process.h"

node * init_process_node(char data, int burst)
{
	node *process = calloc(1, sizeof(node));
	if (!process) {
		fprintf(stderr, "ERROR: Could not allocate memory for process node\n");
		return NULL;
	}

	process->data = data;
	process->burst = burst;
	process->priority = 0;
	process->next = NULL;
	return process;
}


node *init_priority_process_node(char data, int burst, int priority)
{
	node *process = init_process_node(data, burst);
	if (!process) {
		return NULL;
	}
	process->priority = priority;
	return process;
}

void free_processes(node **head)
{
	if (*head) {
		free_processes(&(*head)->next);
	}

	free(*head);
	*head = NULL;
}
