// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "process.h"
#include "queue.h"

Queue *init_queue()
{

	Queue *queue = malloc(sizeof(Queue));
	if (!queue) {
		fprintf(stderr, "ERROR: Could not allocate memory for queue\n");
		return NULL;
	}
	queue->head = NULL;
	queue->tail = NULL;
	return queue;
}

void del_queue(Queue **queue)
{
	if (queue && (*queue)->head) {
		free_processes(&(*queue)->head);
		free((*queue)->head);
		(*queue)->head = NULL;
	}
	free(*queue);
	queue = NULL;
}

void queue_process(Queue *queue, node *process)
{
	if (!queue) {
		return;
	}

	if (!queue->tail) { //empty list
		queue->head = process;
	} else {
		queue->tail->next = process;
	}

	queue->tail = process;
}

node *dqueue_process(Queue *queue)
{
	node *tmp;
	if (!queue || !queue->head) {
		return NULL;
	}
	tmp = queue->head;

	if (queue->head == queue->tail) { // Only one element in the list
		queue->head = queue->tail = NULL;
	} else {
		queue->head = queue->head->next;
	}

	tmp->next = NULL; // isolate the process from the rest of the queue
	return tmp;
}

void print_queue(Queue *queue)
{
	if (!queue || !queue->head) {
		printf("^\n");
	} else {
		node *p = queue->head;
		while (p->next) {
			printf("(%c, %d) --> ", p->data, p->burst);
			p = p->next;
		}
		printf("(%c, %d) ^\n", p->data, p->burst);
	}
}
