// SPDX-License-Identifier: MIT
#ifndef SJF_SCHEDULER_H_
#define SJF_SCHEDULER_H_

int run_sjf_scheduler(void);

#endif
