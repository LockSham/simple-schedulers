// SPDX-License-Identifier: MIT
#ifndef RR_SCHEDULER_H_
#define RR_SCHEDULER_H_

int run_rr_scheduler(void);

#endif
