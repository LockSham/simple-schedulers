// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include "rr-scheduler.h"
#include "fcfs-scheduler.h"
#include "priority-scheduler.h"
#include "sjf-scheduler.h"
#include "sjrf-scheduler.h"

void print_usage(char *program)
{
	fprintf(stderr, "Usage: %s -s SCHEDULER One of fcfs, priority, rr, sjf, sjrf\n", program);
}

int main(int argc, char *argv[])
{
	int opt = 0;
	char *scheduler = "";
	while((opt = getopt(argc, argv, "s:")) != -1) {
		switch (opt) {
			case 's':
				scheduler = optarg;
				break;
			default:
				print_usage(argv[0]);
				return 1;
		}
	}

	if (argc < 3) {
		fprintf(stderr, "No scheduler supplied\n");
		print_usage(argv[0]);
		return 1;
	}

	if (strcmp(scheduler, "fcfs") == 0) {
		return run_fcfs_scheduler();
	} else if (strcmp(scheduler, "priority") == 0) {
		return run_priority_scheduler();
	} else if (strcmp(scheduler, "rr") == 0) {
		return run_rr_scheduler();
	} else if (strcmp(scheduler, "sjf") == 0) {
		return run_sjf_scheduler();
	} else if (strcmp(scheduler, "sjrf") == 0) {
		return run_sjrf_scheduler();
	}

	fprintf(stderr, "Error: Invalid scheduler '%s'\n", scheduler);
	print_usage(argv[0]);
	return 1;
}
