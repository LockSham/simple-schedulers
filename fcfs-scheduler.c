// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "process.h"
#include "queue.h"
#include "fcfs-scheduler.h"

void fcfs_scheduler(Queue *ready, Queue *dispatch)
{
	if (!ready || !dispatch) {
		return;
	}

	node *p = ready->head;
	while (p) {
		node *d_process = calloc(1, sizeof(node));
		memcpy(d_process, p, sizeof(node));
		queue_process(dispatch, d_process);
		p = p->next;
	}
	return;
}

int run_fcfs_scheduler(void)
{
	Queue *ready = init_queue();
	Queue *dispatch = init_queue();
	if (!ready || !dispatch ) {
		fprintf(stderr, "ERROR: Could not initialise ready/dispatch queue\n");
		return -1;
	}
	node *process;
	for (int i = 1; i<6; i++) {
		process = init_process_node(64+i, (int)((double)(99)*rand()/(999999999+1.0)));
		queue_process(ready, process);
	}
	printf("The ready queue for the FCFS scheduler algorithm is \n\n");
	print_queue(ready);
	printf("\n\n");

	fcfs_scheduler(ready, dispatch);

	printf("The dispatch queue for the FCFS scheduler algorithm is \n\n");
	print_queue(dispatch);

	printf("Ending program\n");
	del_queue(&ready);
	del_queue(&dispatch);
	return 0;
}
