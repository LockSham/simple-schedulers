// SPDX-License-Identifier: MIT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "process.h"
#include "queue.h"

void rr_scheduler(Queue *ready, Queue *dispatch, int q)
{
	if (!ready || !dispatch) {
		return;
	}

	while (ready->head) {
		node *process = dqueue_process(ready);
		if (process->burst > 20) {
			process->burst -= q;
			node *dp = init_process_node(process->data, q);
			queue_process(dispatch, dp);
			queue_process(ready, process);
		} else {
			queue_process(dispatch, process);
		}
	}

	return;
}

int run_rr_scheduler(void)
{
	int quantum = 20;
	Queue *ready = init_queue();
	Queue *dispatch = init_queue();
	if (!ready || !dispatch ) {
		fprintf(stderr, "ERROR: Could not initialise ready/dispatch queue\n");
		return -1;
	}

	int burst = 0;
	node *process;
	for (int i = 1; i<6; i++) {
		burst = (int)((double)(99)*rand()/(999999999+1.0));
		process = init_process_node(64+i, burst);
		queue_process(ready, process);
	}
	printf("The ready queue for the RR scheduler algorithm is \n\n");
	print_queue(ready);
	printf("\n\n");

	rr_scheduler(ready, dispatch, quantum);

	printf("The dispatch queue for the RR scheduler algorithm is \n\n");
	print_queue(dispatch);

	printf("\n\nEnding program\n");
	del_queue(&ready);
	del_queue(&dispatch);
	return 0;
}
