// SPDX-License-Identifier: MIT
#ifndef SJRF_SCHEDULER_H_
#define SJRF_SCHEDULER_H_

int run_sjrf_scheduler(void);

#endif
