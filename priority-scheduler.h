// SPDX-License-Identifier: MIT
#ifndef PRIORITY_SCHEDULER_H_
#define PRIORITY_SCHEDULER_H_

int run_priority_scheduler(void);

#endif
